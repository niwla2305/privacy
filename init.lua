minetest.register_chatcommand("delme", {
	params = "",
	description = "Test 1: Modify player's inventory view",
	func = function(name, param)
        --playername = name:get_player_name()
        minetest.show_formspec(name, "privacy:delverify",
        "size[6,2]" ..
        "label[0,0;Are you sure you want to delete your Account on this Server? This will remove everything of you!]" ..
        "label[0,1;!!!Your password, inventories, areas/plots, achievments, etc. will be LOST FOREVER!!!!]" ..
        "button_exit[0,1;2,1;del;Yes, delete this account!]"..
        "button_exit[2,1;2,1;cancel;Nevermind!]")
		return true, "Done."
	end,
})

minetest.register_on_player_receive_fields(function(player, formname, fields)
	if formname == "privacy:delverify" then
        if fields.del then
            local playername = player:get_player_name() 
            minetest.kick_player(playername, "Your account has been succesfully deleted!")
            minetest.after(3.5, function(playername) 
                minetest.remove_player(playername)
                minetest.remove_player_auth(playername)

                if areas then
                    local areaStrings = {}
                    for id, area in pairs(areas.areas) do
                        if areas:isAreaOwner(id, playername) then
                            --table.insert(areaStrings, areas:toString(id))
                            areas:remove(id)
                            areas:save()
                        end
                    end
                end

            end, playername)
        end
	end
end)


--code taken from server_news mod

-- create formspec from text file
local function get_formspec()
	local privacy_file = io.open(minetest.get_worldpath().."/privacy.txt", "r")
    local privacy_fs = 'size[12,8.55]'..
        "label[0,8;I have read and ]"..
        "button_exit[1.6,8;2,1;exit;Accept]"..
        "button[3.6,8;2,1;decline;Decline]"..
        "label[5.55,8;to this privacy policy. ]"
	if privacy_file then
		local privacy = privacy_file:read("*a")
		privacy_file:close()
		privacy_fs = privacy_fs.."textarea[0.25,0;12.1,9;privacy;;"..minetest.formspec_escape(privacy).."]"
	else
		privacy_fs = privacy_fs.."textarea[0.25,0;12.1,9;privacy;;No current privacy.]"
    end
    privacy_fs = privacy_fs.."textarea[0.25,0;12.1,9;privacy;;No current privacy.]"
	return privacy_fs
end

-- show privacy formspec on player join, unless player has bypass priv
minetest.register_on_newplayer(function(player)
	local name = player:get_player_name()
	minetest.show_formspec(name, "privacy", get_formspec())
end)

-- command to display server privacy at any time
minetest.register_chatcommand("privacy", {
	description = "Shows server privacy policy to the player",
	func = function (name)
		local player = minetest.get_player_by_name(name)
		minetest.show_formspec(name, "privacy", get_formspec())	
	end
})

minetest.register_on_player_receive_fields(function(player, formname, fields)
	if formname == "privacy" then
        if fields.decline then
            local playername = player:get_player_name() 
            minetest.kick_player(playername, "Sorry, you have to accept our privacy policy to play on our server.")
        end
	end
end)